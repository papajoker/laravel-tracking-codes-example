<?php
namespace Acme\Repositories;

interface AwbRepositoryInterface {

	public function getAll();
	public function find($code);

}