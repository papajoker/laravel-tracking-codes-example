<?php
namespace Acme\Repositories;
use League\Csv\Reader, AwbCode, Response;

class CSVAwbRepository implements AwbRepositoryInterface {

	protected $csv, $codes, $headers = ['id', 'code', 'deliver'];

	public function __construct()
	{
		$this->csv = new Reader('app/database/awb_codes.csv');
		$this->codes = $this->csv->setOffset(1)->fetchAssoc($this->headers);
	}

	public function getAll()
	{
		return Response::json($this->codes);
	}

	public function find($code)
	{
		
		foreach ($this->codes as $row) {
			if(in_array($code, $row))
				return Response::json($row);
		}

		return Response::json([],404);
	}

}