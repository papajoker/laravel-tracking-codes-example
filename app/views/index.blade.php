@extends('master')
@section('styles')
{{HTML::style('/assets/css/index.css')}}
@stop
@section('content')
{{Form::text('search', null, ['id' => 'search', 'placeholder' => 'enter AWB code here'])}}
{{Form::button('Search', ['id' => 'btn-search'])}}
<div class="deliver-msg"></div>
@stop
@section('scripts')
{{HTML::script('/assets/js/index.js')}}
@stop